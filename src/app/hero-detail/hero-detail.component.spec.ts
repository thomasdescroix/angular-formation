import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HeroService } from '../hero.service';
import { MatDialog } from '@angular/material/dialog';
import { MAX_LENGTH } from '../constants';
import { FormsModule } from '@angular/forms';
import { HeroDetailComponent } from './hero-detail.component';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Hero } from '../models';

describe('HeroDetailComponent', () => {
  let component: HeroDetailComponent;
  let fixture: ComponentFixture<HeroDetailComponent>;
  let location: Location;
  let heroService: HeroService;

  beforeEach(() => {
    const activatedRouteStub = () => ({
      snapshot: { paramMap: { get: () => ({}) } },
    });
    const locationStub = () => ({ back: () => ({}) });
    const heroServiceStub = () => ({
      getHero: (id) => ({ subscribe: (f) => f({}) }),
      updateHero: (hero) => ({ subscribe: (f) => f({}) }),
      deleteHero: (hero) => ({ subscribe: (f) => f({}) }),
    });
    const matDialogStub = () => ({
      open: (confirmDialogComponent, object) => ({
        afterClosed: () => ({ subscribe: (f) => f({}) }),
      }),
    });

    TestBed.configureTestingModule({
      imports: [FormsModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [HeroDetailComponent],
      providers: [
        { provide: ActivatedRoute, useFactory: activatedRouteStub },
        { provide: Location, useFactory: locationStub },
        { provide: HeroService, useFactory: heroServiceStub },
        { provide: MatDialog, useFactory: matDialogStub },
      ],
    });

    fixture = TestBed.createComponent(HeroDetailComponent);
    component = fixture.componentInstance;
    location = fixture.debugElement.injector.get(Location);
    heroService = fixture.debugElement.injector.get(HeroService);
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it(`MAX_LENGTH has default value`, () => {
    expect(component.MAX_LENGTH).toEqual(MAX_LENGTH);
  });

  describe('goBack', () => {
    it('makes expected calls', () => {
      spyOn(location, 'back').and.callThrough();
      component.goBack();
      expect(location.back).toHaveBeenCalled();
    });
  });

  describe('save', () => {
    it('makes expected calls', () => {
      spyOn(component, 'goBack').and.callThrough();
      spyOn(heroService, 'updateHero').and.callThrough();
      component.save();
      expect(heroService.updateHero).toHaveBeenCalled();
      expect(component.goBack).toHaveBeenCalled();
    });
  });

  describe('delete', () => {
    it('makes expected calls', () => {
      // TODO: matDialog confirmation?
      spyOn(component, 'goBack').and.callThrough();
      spyOn(heroService, 'deleteHero').and.callThrough();
      component.delete();
      expect(heroService.deleteHero).toHaveBeenCalled();
      expect(component.goBack).toHaveBeenCalled();
    });
  });

  describe('getHero', () => {
    it('makes expected getHero service call', () => {
      const activatedRouteStub: ActivatedRoute = fixture.debugElement.injector.get(
        ActivatedRoute
      );
      spyOn(heroService, 'getHero').and.callThrough();
      spyOn(activatedRouteStub.snapshot.paramMap, 'get').and.callThrough();
      component.getHero();
      expect(activatedRouteStub.snapshot.paramMap.get).toBeCalledWith('id');
      expect(heroService.getHero).toHaveBeenCalled();
    });

    describe('with hero', () => {
      const heroStub: Hero = {
        id: 123,
        name: 'Alan',
      };
      beforeEach(() => {
        const heroServiceStub = TestBed.inject(HeroService);
        spyOn(heroServiceStub, 'getHero').and.returnValue(of(heroStub));
        fixture.detectChanges();
      });

      it('displays content when initialized', () => {
        const heroDiv = fixture.debugElement.query(By.css('div'));
        expect(heroDiv).toBeTruthy();
      });

      it('has header with hero name in uppercase', () => {
        const header: HTMLHeadingElement = fixture.debugElement.query(
          By.css('h2')
        ).nativeElement;
        expect(header.textContent).toContain(
          `${heroStub.name.toUpperCase()} details`
        );
      });

      it('shows hero id', () => {
        const subtitle: HTMLDivElement = fixture.debugElement.query(
          By.css('mat-card-subtitle')
        ).nativeElement;
        expect(subtitle.textContent).toContain(`Id: ${heroStub.id}`);
      });

      it('has input box with the name', async () => {
        await fixture.whenStable();
        const input: HTMLInputElement = fixture.debugElement.query(
          By.css('input')
        ).nativeElement;
        expect(input.value).toBe(`${heroStub.name}`);
      });

      it('calls location.back() when go back button is clicked', () => {
        spyOn(location, 'back');
        const goBackBtn: HTMLButtonElement = fixture.debugElement.query(
          By.css('button')
        ).nativeElement;
        goBackBtn.click();
        expect(location.back).toHaveBeenCalled();
      });

      it('updates hero property when user types on the input', () => {
        const input: HTMLInputElement = fixture.debugElement.query(
          By.css('input')
        ).nativeElement;
        input.value = 'ABC';
        input.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        expect(component.hero.name).toBe('ABC');
      });

      it('updates hero then goes back when save button is clicked', () => {
        spyOn(heroService, 'updateHero').and.returnValue(of(undefined));
        spyOn(location, 'back');
        const saveBtn: HTMLButtonElement = fixture.debugElement.queryAll(
          By.css('button')
        )[2].nativeElement;
        saveBtn.click();
        expect(heroService.updateHero).toHaveBeenCalledWith(component.hero);
        expect(location.back).toHaveBeenCalled();
      });

      it('deletes hero then goes back when save button is clicked', () => {
        spyOn(heroService, 'deleteHero').and.returnValue(of(undefined));
        spyOn(location, 'back');
        const deleteBtn: HTMLButtonElement = fixture.debugElement.queryAll(
          By.css('button')
        )[1].nativeElement;
        deleteBtn.click();
        expect(heroService.deleteHero).toHaveBeenCalledWith(component.hero);
        expect(location.back).toHaveBeenCalled();
      });
    });
  });

  describe('without a hero', () => {
    it('doesn t display anything when no hero', () => {
      spyOn(heroService, 'getHero').and.returnValue(of(undefined));
      fixture.detectChanges();
      const heroDiv = fixture.debugElement.query(By.css('div'));
      expect(heroDiv).toBeFalsy();
    });
  });

  describe('ngOnInit', () => {
    it('makes expected getHero call', () => {
      spyOn(component, 'getHero').and.callThrough();
      component.ngOnInit();
      expect(component.getHero).toHaveBeenCalled();
    });
  });
});
