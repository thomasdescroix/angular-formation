import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Hero, ConfirmDialogConfig } from '../models';
import { HeroService } from '../hero.service';
import { MAX_LENGTH } from '../constants';
import { ConfirmDialogComponent } from '../shared/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'toh-hero-detail',
  templateUrl: './hero-detail.component.html'
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero;
  readonly MAX_LENGTH = MAX_LENGTH; // of hero's name input

  /**
   * Returns if save button is disabled.
   * @readonly
   * @type {boolean}
   */
  get isDisabled(): boolean {
    return Boolean(!this.hero.name.length);
  }

  constructor(
    private readonly route: ActivatedRoute,
    private readonly heroService: HeroService,
    private readonly location: Location,
    private readonly matDialog: MatDialog
  ) {}

  ngOnInit() {
    this.getHero();
  }

  /**
   * Get a specific hero with route parameter id.
   */
  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id).subscribe(hero => (this.hero = hero));
  }

  /**
   * Navigates backward one step in the browser's history stack using the Location service.
   */
  goBack(): void {
    this.location.back();
  }

  /**
   * Update a specific hero.
   */
  save(): void {
    this.heroService.updateHero(this.hero).subscribe(() => this.goBack());
  }

  /**
   * Delete specific hero after confirmation.
   */
  delete(): void {
    const data: ConfirmDialogConfig = {
      message: 'Are you sure you want to delete this hero?',
      cancel: 'CANCEL',
      confirm: 'DELETE'
    };
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      data
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.heroService.deleteHero(this.hero).subscribe(() => this.goBack());
      }
    });
  }
}
