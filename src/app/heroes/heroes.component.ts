import { Component, OnInit } from '@angular/core';
import { Hero } from '../models';
import { HeroService } from '../hero.service';
import { MatDialog } from '@angular/material/dialog';
import { NewHeroDialogComponent } from '../new-hero-dialog/new-hero-dialog.component';

@Component({
  selector: 'toh-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];

  constructor(
    private readonly heroService: HeroService,
    private readonly matDialog: MatDialog
  ) {}

  ngOnInit() {
    this.getHeroes();
  }

  /**
   * Get the heroes from the HeroService
   */
  getHeroes(): void {
    this.heroService.getHeroes().subscribe(heroes => (this.heroes = heroes));
  }

  /**
   * Open new creation hero dialog.
   * Reload heroes after closed only if new hero was created.
   */
  add(): void {
    const dialogRef = this.matDialog.open(NewHeroDialogComponent);
    dialogRef.afterClosed().subscribe(hero => {
      if (hero) {
        this.getHeroes();
      }
    });
  }

  trackByFn(hero: Hero): number {
    return hero.id;
  }
}
