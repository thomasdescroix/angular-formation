import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Hero } from '../models';
import { HeroService } from '../hero.service';
import { MatDialog } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroesComponent } from './heroes.component';

describe('HeroesComponent', () => {
  let component: HeroesComponent;
  let fixture: ComponentFixture<HeroesComponent>;

  beforeEach(() => {
    const heroStub = () => ({ id: {} });
    const heroServiceStub = () => ({
      getHeroes: () => ({ subscribe: (f) => f({}) }),
    });
    const matDialogStub = () => ({
      open: (newHeroDialogComponent) => ({
        afterClosed: () => ({ subscribe: (f) => f({}) }),
      }),
    });

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [HeroesComponent],
      providers: [
        { provide: Hero, useFactory: heroStub },
        { provide: HeroService, useFactory: heroServiceStub },
        { provide: MatDialog, useFactory: matDialogStub },
      ],
    });

    fixture = TestBed.createComponent(HeroesComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      spyOn(component, 'getHeroes').and.callThrough();
      component.ngOnInit();
      expect(component.getHeroes).toHaveBeenCalled();
    });
  });

  describe('getHeroes', () => {
    it('makes expected calls', () => {
      const heroServiceStub: HeroService = fixture.debugElement.injector.get(
        HeroService
      );
      spyOn(heroServiceStub, 'getHeroes').and.callThrough();
      component.getHeroes();
      expect(heroServiceStub.getHeroes).toHaveBeenCalled();
    });
  });

  describe('add', () => {
    // TODO: matDialog hero ?
    it('makes expected calls', () => {
      const matDialogStub: MatDialog = fixture.debugElement.injector.get(
        MatDialog
      );
      spyOn(component, 'getHeroes').and.callThrough();
      spyOn(matDialogStub, 'open').and.callThrough();
      component.add();
      expect(component.getHeroes).toHaveBeenCalled();
      expect(matDialogStub.open).toHaveBeenCalled();
    });
  });

  describe('trackByFn', () => {
    it('must return hero id', () => {
      const heroStub: Hero = TestBed.inject(Hero);
      expect(component.trackByFn(heroStub)).toEqual(heroStub.id);
    });
  });
});
