import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Hero } from './models';
import { SnackbarService } from './shared/snackbar/snackbar.service';

@Injectable({ providedIn: 'root' })
export class HeroService {
  private readonly heroesUrl = 'api/heroes';
  private readonly httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private readonly http: HttpClient,
    private readonly snackbarService: SnackbarService
  ) {}

  /**
   * (GET) Get heroes from the server
   *
   * @returns {Observable<Hero[]>} observable of heroes
   */
  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.heroesUrl).pipe(
      tap(_ => this.log('fetched heroes')),
      catchError(this.handleError<Hero[]>('getHeroes', []))
    );
  }

  /**
   * (GET) Get specific hero by id from the server
   *
   * @param {number} id identifiant of fetched hero
   * @returns {Observable<Hero>} observable of hero
   */
  getHero(id: number): Observable<Hero> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Hero>(url).pipe(
      tap(_ => this.log(`fetched hero id=${id}`)),
      catchError(this.handleError<Hero>(`getHero id=${id}`))
    );
  }

  /**
   * (PUT) Update the hero on the server
   *
   * @param {Hero} hero updated hero
   * @returns {Observable<any>} observable
   */
  updateHero(hero: Hero): Observable<any> {
    return this.http.put(this.heroesUrl, hero, this.httpOptions).pipe(
      tap(_ => this.log(`updated hero id=${hero.id}`)),
      catchError(this.handleError<any>('updateHero'))
    );
  }

  /**
   * (POST) Add a new hero to the sever
   *
   * @param {Hero} hero added hero
   * @returns {Observable<Hero>} observable of the new hero
   */
  addHero(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.heroesUrl, hero, this.httpOptions).pipe(
      tap((newHero: Hero) => this.log(`added hero w/ id=${newHero.id}`)),
      catchError(this.handleError<Hero>('addHero'))
    );
  }

  /**
   * (DELETE) Delete the hero from the server
   *
   * @param {(Hero | number)} hero deleted hero or hero id
   * @returns {Observable<Hero>} observable of hero
   */
  deleteHero(hero: Hero | number): Observable<Hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;

    return this.http.delete<Hero>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero'))
    );
  }

  /**
   * (GET) Get heroes whose name contains search term
   *
   * @param {string} term searched term
   * @returns {Observable<Hero[]>} observable of heroes
   */
  searchHeroes(term: string): Observable<Hero[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array
      return of([]);
    }
    return this.http.get<Hero[]>(`${this.heroesUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`found heroes matching "${term}"`)),
      catchError(this.handleError<Hero[]>('searchHeroes', []))
    );
  }

  /**
   * Log a HeroService message with the SnackbarService
   *
   * @private
   * @param {string} message HeroService message
   */
  private log(message: string) {
    this.snackbarService.openSnackBar(`HeroService: ${message}`);
  }

  /**
   * Handle Http operation that failed and let the app continue.
   *
   * @private
   * @template T
   * @param {string} [operation='operation'] name of the operation that failed
   * @param {T} [result] optional value to return as the observable result
   * @returns
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result);
    };
  }
}
