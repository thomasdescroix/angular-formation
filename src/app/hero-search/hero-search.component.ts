import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Hero } from '../models';
import { HeroService } from '../hero.service';

@Component({
  templateUrl: './hero-search.component.html'
})
export class HeroSearchComponent implements OnInit {
  heroes$: Observable<Hero[]>;
  private readonly searchTerms = new Subject<string>();

  constructor(private readonly heroService: HeroService) {}

  ngOnInit() {
    const dueTime = 300;
    this.heroes$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(dueTime),
      // ignore new term if same as previous term
      distinctUntilChanged(),
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.heroService.searchHeroes(term))
    );
  }

  /**
   * Push a search term into the observable straem.
   *
   * @param {string} term searched term
   */
  search(term: string): void {
    this.searchTerms.next(term);
  }
}
