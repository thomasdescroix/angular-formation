import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
  discardPeriodicTasks,
} from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HeroService } from '../hero.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroSearchComponent } from './hero-search.component';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { Hero } from '../models';

describe('HeroSearchComponent', () => {
  let component: HeroSearchComponent;
  let fixture: ComponentFixture<HeroSearchComponent>;

  beforeEach(() => {
    const heroServiceStub = () => ({ searchHeroes: (term) => ({}) });
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [HeroSearchComponent],
      providers: [{ provide: HeroService, useFactory: heroServiceStub }],
    });

    fixture = TestBed.createComponent(HeroSearchComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it('starts with an emty list', () => {
    fixture.detectChanges();
    const heroLinks: Array<HTMLAnchorElement> = fixture.debugElement
      .queryAll(By.css('a'))
      .map((a) => a.nativeElement);
    expect(heroLinks.length).toBe(0);
  });

  it('typing on the input box doesn’t change the list for 299ms', fakeAsync(() => {
    const heroStub: Hero = { id: 1, name: 'Alan' };
    fixture.detectChanges();
    const heroServiceStub: HeroService = TestBed.inject(HeroService);
    spyOn(heroServiceStub, 'searchHeroes').and.returnValue(of([heroStub]));

    const input: HTMLInputElement = fixture.debugElement.query(By.css('input'))
      .nativeElement;
    input.value = 'A';
    input.dispatchEvent(new Event('input'));
    tick(299);
    fixture.detectChanges();

    const herolinks: Array<HTMLAnchorElement> = fixture.debugElement
      .queryAll(By.css('a'))
      .map((a) => a.nativeElement);

    expect(herolinks.length).toBe(0);
    // Prevent Error: 1 periodic timer(s) still in the queue.
    discardPeriodicTasks();
  }));

  it('the list of matching heroes appears after 300ms', fakeAsync(() => {
    const heroesStub: Hero[] = [
      { id: 1, name: 'Alan' },
      { id: 2, name: 'Alexander' },
    ];
    fixture.detectChanges();
    const heroServiceStub: HeroService = TestBed.inject(HeroService);
    spyOn(heroServiceStub, 'searchHeroes').and.returnValue(of(heroesStub));

    const input: HTMLInputElement = fixture.debugElement.query(By.css('input'))
      .nativeElement;
    input.value = 'Al';
    input.dispatchEvent(new Event('input'));
    tick(300);
    fixture.detectChanges();
    const heroLinks: Array<HTMLAnchorElement> = fixture.debugElement
      .queryAll(By.css('a'))
      .map((a) => a.nativeElement);
    expect(heroLinks.length).toBe(2);
    expect(heroLinks[0].textContent).toContain(heroesStub[0].name);
    expect(heroLinks[1].textContent).toContain(heroesStub[1].name);
  }));

  it('can perform multiple searches in a row - 300ms apart', fakeAsync(() => {
    const heroesStub: Hero[] = [
      { id: 1, name: 'Alan' },
      { id: 2, name: 'Alexander' },
      { id: 3, name: 'Thomas' },
    ];
    fixture.detectChanges();
    const heroServiceStub: HeroService = TestBed.inject(HeroService);

    spyOn(heroServiceStub, 'searchHeroes').and.callFake((term) => {
      return of(heroesStub.filter((hero) => hero.name.includes(term)));
    });

    const input: HTMLInputElement = fixture.debugElement.query(By.css('input'))
      .nativeElement;

    // Search for the first time
    input.value = 'Al';
    input.dispatchEvent(new Event('input'));
    tick(300);

    // Narrow the search
    input.value = 'Alan';
    input.dispatchEvent(new Event('input'));
    tick(300);
    fixture.detectChanges();

    const heroLinks: Array<HTMLAnchorElement> = fixture.debugElement
      .queryAll(By.css('a'))
      .map((a) => a.nativeElement);
    expect(heroLinks.length).toBe(1);
    expect(heroLinks[0].textContent).toContain('Alan');
    expect(heroLinks[0].getAttribute('href')).toBe('/detail/1');
  }));

  it('doesn t perform a search if the search term doesn t change', fakeAsync(() => {
    fixture.detectChanges();
    const heroServiceStub: HeroService = TestBed.inject(HeroService);

    spyOn(heroServiceStub, 'searchHeroes').and.callFake((term) => {
      return of(
        [
          { id: 1, name: 'Alan' },
          { id: 2, name: 'Alexander' },
          { id: 3, name: 'Thomas' },
        ].filter((hero) => hero.name.includes(term))
      );
    });

    const input: HTMLInputElement = fixture.debugElement.query(By.css('input'))
      .nativeElement;

    // Search for the first time
    input.value = 'Al';
    input.dispatchEvent(new Event('input'));
    tick(300);

    // Trigger the input's change event again
    input.dispatchEvent(new Event('input'));
    tick(300);
    fixture.detectChanges();

    expect(heroServiceStub.searchHeroes).toHaveBeenCalledTimes(1);
  }));
});
