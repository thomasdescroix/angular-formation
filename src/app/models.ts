export class Hero {
  id: number;
  name: string;
}

export interface ConfirmDialogConfig {
  title?: string;
  message: string;
  confirm: string;
  cancel: string;
}
