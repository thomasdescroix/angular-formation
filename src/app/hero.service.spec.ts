import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Hero } from './models';
import { SnackbarService } from './shared/snackbar/snackbar.service';
import { HeroService } from './hero.service';

describe('HeroService', () => {
  let hero: Hero;
  let service: HeroService;
  let snackbarService: SnackbarService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    const heroStub = () => ({ id: {} });
    const snackbarServiceStub = () => ({ openSnackBar: (message) => ({}) });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        HeroService,
        { provide: Hero, useFactory: heroStub },
        { provide: SnackbarService, useFactory: snackbarServiceStub },
      ],
    });

    service = TestBed.inject(HeroService);
    httpTestingController = TestBed.inject(HttpTestingController);
    hero = TestBed.inject(Hero);
    snackbarService = TestBed.inject(SnackbarService);

    spyOn(snackbarService, 'openSnackBar').and.callThrough();
    spyOn(console, 'error');
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  describe('getHeroes', () => {
    const url = 'api/heroes';

    it('makes expected http get call and returns heroes', () => {
      service.getHeroes().subscribe((res) => {
        expect(res).toEqual([hero]);
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('GET');
      req.flush([]);
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        'HeroService: fetched heroes'
      );
    });

    it('handles 404 error', () => {
      service.getHeroes().subscribe((res) => {
        expect(res).toEqual([]);
      });

      const req = httpTestingController.expectOne(url);
      req.flush('Error', { status: 404, statusText: 'Not Found' });
      expect(console.error).toHaveBeenCalled();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        'HeroService: getHeroes failed: Http failure response for api/heroes: 404 Not Found'
      );
    });
  });

  describe('getHero', () => {
    const id = 123;
    const url = `api/heroes/${id}`;

    it('makes expected http get call and returns specific hero', () => {
      service.getHero(id).subscribe((res) => {
        expect(res).toEqual(hero);
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('GET');
      req.flush(hero);
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: fetched hero id=${id}`
      );
    });

    it('handles 404 error', () => {
      service.getHero(id).subscribe((res) => {
        expect(res).toBeUndefined();
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('GET');
      req.flush('Error', { status: 404, statusText: 'Not Found' });
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: getHero id=${id} failed: Http failure response for api/heroes/${id}: 404 Not Found`
      );
    });
  });

  describe('updateHero', () => {
    const url = 'api/heroes';

    it('makes expected http put call and returns updated hero', () => {
      service.updateHero(hero).subscribe((res) => {
        expect(res).toEqual(hero);
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('PUT');
      req.flush(hero);
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: updated hero id=${hero.id}`
      );
    });

    it('handles 404 error', () => {
      service.updateHero(hero).subscribe((res) => {
        expect(res).toBeUndefined();
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('PUT');
      req.flush('Error', { status: 404, statusText: 'Not Found' });
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        'HeroService: updateHero failed: Http failure response for api/heroes: 404 Not Found'
      );
    });
  });

  describe('addHero', () => {
    const url = 'api/heroes';

    it('makes expected http post call and returns new hero', () => {
      service.addHero(hero).subscribe((res) => {
        expect(res).toEqual(hero);
      });

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('POST');
      req.flush(hero);
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: added hero w/ id=${hero.id}`
      );
    });

    it('handles 404 error', () => {
      service.addHero(hero).subscribe((res) => {
        expect(res).toBeUndefined();
      });

      const req = httpTestingController.expectOne(url);
      req.flush('Error', { status: 404, statusText: 'Not Found' });
      expect(console.error).toHaveBeenCalled();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        'HeroService: addHero failed: Http failure response for api/heroes: 404 Not Found'
      );
    });
  });

  describe('deleteHero', () => {
    it('makes expected http delete call and returns deleted hero', () => {
      service.deleteHero(hero).subscribe((res) => {
        expect(res).toEqual(hero);
      });

      const req = httpTestingController.expectOne(`api/heroes/${hero.id}`);
      expect(req.request.method).toEqual('DELETE');
      req.flush(hero);
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: deleted hero id=${hero.id}`
      );
    });

    it('makes expected http delete by id call and returns deleted hero', () => {
      const id = 123;
      service.deleteHero(id).subscribe((res) => {
        expect(res).toEqual(hero);
      });

      const req = httpTestingController.expectOne(`api/heroes/${id}`);
      expect(req.request.method).toEqual('DELETE');
      req.flush(hero);
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: deleted hero id=${id}`
      );
    });

    it('handles 404 error', () => {
      service.deleteHero(hero).subscribe((res) => {
        expect(res).toBeUndefined();
      });

      const req = httpTestingController.expectOne(`api/heroes/${hero.id}`);
      expect(req.request.method).toEqual('DELETE');
      req.flush('Error', { status: 404, statusText: 'Not Found' });
      expect(console.error).toHaveBeenCalled();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: deleteHero failed: Http failure response for api/heroes/${hero.id}: 404 Not Found`
      );
    });
  });

  describe('searchHeroes', () => {
    it('returns empty array if search term is blank and doesn t make http call', () => {
      service.searchHeroes('').subscribe((res) => {
        expect(res).toEqual([]);
      });

      httpTestingController.expectNone('api/heroes/?name=');
      httpTestingController.verify();
    });

    it('returns heroes by search term using http GET', () => {
      const heroes: Array<Hero> = [hero];
      const searchTerms = 'abc';
      service.searchHeroes(searchTerms).subscribe((res) => {
        expect(res).toEqual(heroes);
      });

      const req = httpTestingController.expectOne(
        `api/heroes/?name=${searchTerms}`
      );
      expect(req.request.method).toEqual('GET');
      req.flush(heroes);
      httpTestingController.verify();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: found heroes matching \"${searchTerms}\"`
      );
    });

    it('handles 404 error', () => {
      const searchTerms = 'abc';
      service.searchHeroes(searchTerms).subscribe((res) => {
        expect(res).toEqual([]);
      });

      const req = httpTestingController.expectOne(
        `api/heroes/?name=${searchTerms}`
      );
      req.flush('Error', { status: 404, statusText: 'Not Found' });
      expect(console.error).toHaveBeenCalled();

      expect(snackbarService.openSnackBar).toHaveBeenCalledWith(
        `HeroService: searchHeroes failed: Http failure response for api/heroes/?name=${searchTerms}: 404 Not Found`
      );
    });
  });
});
