import { Injectable } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarRef,
  SimpleSnackBar
} from '@angular/material/snack-bar';
import { DEFAULT_SNACKBAR_DURATION } from 'src/app/constants';

@Injectable()
export class SnackbarService {
  constructor(private snackbar: MatSnackBar) {}

  openSnackBar(
    message: string,
    duration = DEFAULT_SNACKBAR_DURATION
  ): MatSnackBarRef<SimpleSnackBar> {
    return this.snackbar.open(message, undefined, {
      duration
    });
  }
}
