import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarService } from './snackbar.service';

describe('SnackbarService', () => {
  let service: SnackbarService;

  beforeEach(() => {
    const matSnackBarStub = () => ({
      open: (message, action, config) => ({}),
    });

    TestBed.configureTestingModule({
      providers: [
        SnackbarService,
        { provide: MatSnackBar, useFactory: matSnackBarStub },
      ],
    });

    service = TestBed.inject(SnackbarService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  describe('openSnackBar', () => {
    it('opens a snackbar with a message', () => {
      const message = 'message';
      const matSnackBar = TestBed.inject(MatSnackBar);
      spyOn(matSnackBar, 'open');

      service.openSnackBar(message);
      expect(matSnackBar.open).toHaveBeenCalled();
    });
  });
});
