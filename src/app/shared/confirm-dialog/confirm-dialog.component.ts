import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogConfig } from 'src/app/models';

@Component({
  template: `
    <h2 matDialogTitle *ngIf="config.title">{{ config.title }}</h2>
    <mat-dialog-content>
      {{ config.message }}
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-raised-button [mat-dialog-close]="false" tabindex="-1">
        <mat-icon>cancel</mat-icon> {{ config.cancel }}
      </button>
      <button
        type="submit"
        mat-raised-button
        color="primary"
        [mat-dialog-close]="true"
        tabindex="-1"
      >
        <mat-icon>check_circle</mat-icon> {{ config.confirm }}
      </button>
    </mat-dialog-actions>
  `
})
export class ConfirmDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public config: ConfirmDialogConfig
  ) {}
}
