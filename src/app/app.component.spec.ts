import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterLinkWithHref } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AppComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeDefined();
  });

  it(`should have as title 'Tour of Heroes'`, () => {
    expect(component.title).toEqual('Tour of Heroes');
  });

  it('should render title in a h1 tag', () => {
    const h1: HTMLElement = fixture.debugElement.nativeElement.querySelector(
      'h1'
    );
    expect(h1.textContent).toContain('Tour of Heroes');
  });

  it('should render router links', () => {
    const routerLinks = fixture.debugElement.queryAll(
      By.directive(RouterLinkWithHref)
    );
    expect(routerLinks.length).toBe(3);
    expect(routerLinks[0].attributes.href).toBe('/dashboard');
    expect(routerLinks[1].attributes.href).toBe('/heroes');
    expect(routerLinks[2].attributes.href).toBe('/heroes/search');
  });
});
