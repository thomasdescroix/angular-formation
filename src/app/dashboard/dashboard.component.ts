import { Component, OnInit } from '@angular/core';
import { Hero } from '../models';
import { HeroService } from '../hero.service';

@Component({
  selector: 'toh-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];

  constructor(private readonly heroService: HeroService) {}

  ngOnInit() {
    this.getHeroes();
  }

  /**
   * Returns the sliced list of heroes at posititions 0 and 4.
   */
  getHeroes(): void {
    const end = 5;
    this.heroService
      .getHeroes()
      .subscribe(heroes => (this.heroes = heroes.slice(0, end)));
  }

  trackByFn(hero: Hero): number {
    return hero.id;
  }
}
