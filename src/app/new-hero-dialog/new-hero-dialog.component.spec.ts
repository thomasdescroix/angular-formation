import { ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { HeroService } from '../hero.service';
import { FormBuilder } from '@angular/forms';
import { MAX_LENGTH } from '../constants';
import { NewHeroDialogComponent } from './new-hero-dialog.component';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('NewHeroDialogComponent', () => {
  let component: NewHeroDialogComponent;
  let fixture: ComponentFixture<NewHeroDialogComponent>;

  beforeEach(() => {
    const matDialogRefStub = () => ({ close: (hero) => ({}) });
    const heroServiceStub = () => ({
      addHero: (hero) => ({ subscribe: (f) => f({}) }),
    });
    const formBuilderStub = () => ({ group: (object) => ({}) });

    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [NewHeroDialogComponent],
      providers: [
        { provide: MatDialogRef, useFactory: matDialogRefStub },
        { provide: HeroService, useFactory: heroServiceStub },
        { provide: FormBuilder, useFactory: formBuilderStub },
      ],
    });

    fixture = TestBed.createComponent(NewHeroDialogComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it('MAX_LENGTH has default value', () => {
    expect(component.MAX_LENGTH).toEqual(MAX_LENGTH);
  });
});
