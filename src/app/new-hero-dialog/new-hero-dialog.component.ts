import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Hero } from 'src/app/models';
import { MAX_LENGTH } from '../constants';
import { HeroService } from '../hero.service';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';

@Component({
  templateUrl: './new-hero-dialog.component.html'
})
export class NewHeroDialogComponent {
  readonly MAX_LENGTH = MAX_LENGTH;
  newHeroForm: FormGroup;
  nameControl: FormControl;

  constructor(
    public dialogRef: MatDialogRef<NewHeroDialogComponent>,
    private fb: FormBuilder,
    private readonly heroService: HeroService
  ) {
    this.newHeroForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(MAX_LENGTH)]]
    });
  }

  /**
   * Create new hero, then close the dialog
   * @param {{ name: string }} formData
   */
  add(formData: { name: string }): void {
    let name = formData.name;
    name = name.trim();
    if (this.newHeroForm.dirty && this.newHeroForm.valid) {
      this.heroService.addHero({ name } as Hero).subscribe((hero: Hero) => {
        this.newHeroForm.reset();
        this.dialogRef.close(hero);
      });
    } else {
      return;
    }
  }
}
